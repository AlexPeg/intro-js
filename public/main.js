import * as exercice1 from './exo-1.js'
exercice1.myFunction();

import * as exercice2 from './exo-2.js'
exercice2.myFunction2();

import * as exercice3 from './exo-3.js'
exercice3.myFunction3();

import * as exercice4 from './exo-4.js'
exercice4.myFunction4();

import * as exercice5 from './exo-5.js'
exercice5.myFunction5();

import * as exercice6 from './exo-6.js'
exercice6.myFunction6();

import * as exercice7 from './exo-7.js'
exercice7.myFunction7();

import * as exercice8 from './exo-8.js'
exercice8.myFunction8();

import * as fonctions from './fonction.js'
fonctions.myFirstFunction();
fonctions.myFunction('Un, deux, trois');
fonctions.myFunction2();
fonctions.myFunction3();
fonctions.myFunction4(13, "Coco");
fonctions.myFunction5("Pégourie", "Alex", 32);
fonctions.myFunction6("homme", 18);
fonctions.myFunction7(7, 9, 100);

import * as tableaux from './tableau.js'
tableaux.Array();