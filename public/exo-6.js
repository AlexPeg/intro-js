//Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...

export const myFunction6 = () => {
    for (let nombre2 = 20; nombre2 > 0; nombre2--) {
        console.log(nombre2 + " C'est presque bon...")
    }
}