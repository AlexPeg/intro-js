//Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

export const myFunction7 = () => {
    for (let nombre3 = 1; nombre3 <= 100; nombre3 += 15) {
        console.log(nombre3 + " On tient le bon bout...")
    }
}