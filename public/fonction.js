//Exercice 1 Faire une fonction qui retourne true.

export const myFirstFunction = () => {
    console.log("true");
}

//Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.

export const myFunction = (chainedearacteres) => {
    console.log(chainedearacteres);
}

//Exercice 3 Faire une fonction qui prend en paramètre deux chaines de caractères et qui renvoit la concaténation de ces deux chaines.
let name = "toto";
let age = 13;
export const myFunction2 = () => {
    console.log(name + '' + age);
}

/*Exercice 4 Faire une fonction qui prend en paramètre deux nombres. La fonction doit retourner :

    Le premier nombre est plus grand si le premier nombre est plus grand que le deuxième
    Le premier nombre est plus petit si le premier nombre est plus petit que le deuxième
    Les deux nombres sont identiques si les deux nombres sont égaux */

let number1;
let number2;
export const myFunction3 = (number1 = 10, number2 = 80) => {
    if (number1 > number2) {
        console.log("Le premier nombre est plus grand")
    }
    if (number1 < number2) {
        console.log("Le premier nombre est plus petit")
    }
    if (number1 === number2)
        console.log("Les deux nombres sont identiques")
}

//Exercice 5 Faire une fonction qui prend en paramètre un nombre et une chaine de caractères et qui renvoit la concaténation de ces deux paramètres.

export const myFunction4 = (nombre, str) => {
    console.log(nombre + " " + str);
}

//Exercice 6 Faire une fonction qui prend trois paramètres : nom, prenom et age. Elle doit renvoyer une chaine de la forme : "Bonjour" + nom + prenom + ", tu as " + age + "ans".

export const myFunction5 = (nom, prenom, age) => {
    console.log("Bonjour" + " " + nom + " " + prenom, "tu as" + " " + age + " " + "ans.")
}

/*Exercice 7 Faire une fonction qui prend deux paramètres : age et genre. Le paramètre genre peut prendre comme valeur Homme ou Femme. La fonction doit renvoyer en fonction des paramètres (gérer tous les cas) :

    Vous êtes un homme et vous êtes majeur
    Vous êtes un homme et vous êtes mineur
    Vous êtes une femme et vous êtes majeur
    Vous êtes une femme et vous êtes mineur */

export const myFunction6 = (genre, age2) => {
    if (genre == "homme" && age2 >= 18) {
        console.log("Vous etes un homme et vous etes majeur");
    }
    if (genre == "homme" && age2 < 18) {
        console.log("Vous etes un homme et vous etes mineur");
    }
    if (genre == "femme" && age2 >= 18) {
        console.log("Vous etes une femme et vous etes majeure");
    }
    if (genre == "femme" && age2 < 18)
        console.log("Vous etes une femme et vous etes mineure");
}

//Exercice 8 Faire une fonction qui prend en paramètre trois nombres et qui renvoit la somme de ces nombres. Tous les paramètres doivent avoir une valeur par défaut.